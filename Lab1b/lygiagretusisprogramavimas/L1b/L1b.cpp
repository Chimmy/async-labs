#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include "include/json.hpp"

using namespace std;
using json = nlohmann::json;

namespace ns 
{
    struct Pet 
	{
        string type;
        int age;
        double cost;
    };

    struct Pets 
	{
        vector<Pet> pets;
    };

    void from_json(const json& j, Pet& p) 
	{
        j.at("Type").get_to(p.type);
        j.at("Cost").get_to(p.cost);
        j.at("Age").get_to(p.age);
    }

    void from_json(const json& j, Pets& p) 
	{
        j.at("Pets").get_to(p.pets);
    }
}

ns::Pet NULL_PTR;
const string FILE_NAME = "IFF_7-11_AdomaitisP_L1_dat_3.json";
const int DATASET_MONITOR_SIZE = 10;
const int THREAD_COUNT = 5;

ns::Pets read() 
{
	ifstream i(FILE_NAME);
	json j;
	i >> j;

	return j.get<ns::Pets>();
}

class Monitor 
{
	private:
		ns::Pet* pets;
		int count = 0;
		int capacity = 0;
		bool inserting = true;

	public:
		Monitor(int size) 
		{
			capacity = size;
			pets = new ns::Pet[capacity];
			inserting = true;
			count = 0;
		}

		~Monitor() 
		{
			delete[] pets;
		}

		ns::Pet get() 
		{
			ns::Pet result;
			while (count < 1 && inserting) {}
			#pragma omp critical
			{
				if (count > 0) 
				{
					result = pets[count - 1];
					pets[count - 1] = NULL_PTR;
					count--;
				}
				else if (!inserting) 
				{
					result = NULL_PTR;
				}
			}
			return result;
		}

		void put(ns::Pet pet) 
		{
			while (count >= capacity) {}
			#pragma omp critical
			{
				pets[count] = pet;
				count++;
			}
		}

		void putSorted(ns::Pet pet) 
		{
			while (count >= capacity) {}
			#pragma omp critical
			{
				int i = 0;
				while (pets[i].type.compare(pet.type) < 0 && i < count) i++;
				while (i < count) {
					swap(pets[i], pet);
					i++;
				}
				pets[count] = pet;
				count++;
			}
		}

		void onDoneInserting() 
		{
			inserting = false;
		}

		void addToResults(string fileName) 
		{
			ofstream fs(fileName);

			fs << string(70, '-') << endl;
			fs << "| " << setw(40) << "FULL NAME"
				<< " | " << setw(3) << "AGE"
				<< " | " << setw(11) << "COST"
				<< " |" << endl;

			fs << string(70, '-') << endl;
			for (int i = 0; i < count; i++)
				fs << "| " << setw(40) << pets[i].type
				<< " | " << setw(3) << pets[i].age
				<< " | " << setw(11) << pets[i].cost
				<< " |" << endl;
		}
};

void addToData(Monitor* dataset, ns::Pets pets) 
{

	for (int i = 0; i < pets.pets.size(); i++) {
		dataset->put(pets.pets[i]);
	}
	dataset->onDoneInserting();
}

void filterPets(Monitor* dataset, Monitor* results) 
{
	while (true) {
		ns::Pet pet = dataset->get();
		pet.age = pet.age * pet.age;
		if (pet.type.compare(NULL_PTR.type) == 0) {
			break;
		}
		if (pet.type.compare("") == 0) {
			continue;
		}
		if (pet.age > 8) {
			results->putSorted(pet);
		}
	}
}

void createThreads(Monitor* dataset, Monitor* results, ns::Pets pets) 
{
#pragma omp parallel
	{
		if (omp_get_thread_num() == THREAD_COUNT - 1) {
			printf("Adder thread %d started\n", omp_get_thread_num());
			addToData(dataset, pets);
		}
		else {
			printf("Worker thread %d started\n", omp_get_thread_num());
			filterPets(dataset, results);
		}
	}
}

int main()
{
	NULL_PTR.type = "EMPTY_RECORD";

	ns::Pets pets = read();
	Monitor* data = new Monitor(DATASET_MONITOR_SIZE);
	Monitor* results = new Monitor(pets.pets.size());

	omp_set_num_threads(THREAD_COUNT);
	createThreads(data, results, pets);

	results->addToResults("output.txt");

	delete(results);
	delete(data);
	
	return 0;
}
