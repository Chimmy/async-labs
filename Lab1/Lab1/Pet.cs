﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Lab1
{
    public class ResultMonitor
    {
        private Predicate<Pet> pr = null;
        private object threadLock = new object();
        private Pet[] WorkingArray = new Pet[50];
        private Pet[] Results = new Pet[25];
        private int index;
        private int resultIndex;

        public ResultMonitor(Predicate<Pet> pred)
        {
            pr = pred;
        }

        public void Add(Pet obj)
        {
            lock (threadLock)
            {
                WorkingArray[index++] = obj;
            }
        }

        public void MoveToResults(int i)
        {
            while (WorkingArray.Any(x => x != null))
            {
                lock (threadLock)
                {
                    if (resultIndex < WorkingArray.Length && WorkingArray[resultIndex] != null)
                    {
                        WorkingArray[resultIndex].OperationResult = WorkingArray[resultIndex].Age * WorkingArray[resultIndex].Age;
                        if (pr.Invoke(WorkingArray[resultIndex]))
                        {
                            Console.WriteLine($"Thread {i} is writing stuff");
                            InsertToCorrectSpot(WorkingArray[resultIndex], 0);
                            WorkingArray[resultIndex++] = null;
                        }
                        else
                        {
                            WorkingArray[resultIndex++] = null;
                        }
                    }
                }
                Console.WriteLine($"Thread {i} is sleeping");
                Thread.Sleep(500);
            }
            
        }

        private void InsertToCorrectSpot(Pet pet, int i)
        {
            if (i == Results.Length)
            {
                return;
            }

            if (Results[i] == null)
            {
                Results[i] = pet;
            }

            if (String.Compare(Results[i].Type, pet.Type) > 0)
            {
                var tmp = Results[i];
                Results[i] = pet;
                InsertToCorrectSpot(tmp, ++i);
            }
            else
            {
                InsertToCorrectSpot(pet, ++i);
            }
        }

        public string FormatResults()
        {
            var res = "No results satisfy the chosen predicate";
            if (Results.All(x => x == null))
            {
                Console.WriteLine(res);
                return res;
            }

            res = Results
                .Where(x => x != null)
                .Distinct()
                .Select(x => x.Format())
                .Aggregate((s, t) => s + (Environment.NewLine + t));
            Console.WriteLine(res);
            return res;
        }
    }

    public class Pet
    {
        public string Type { get; set; }
        public int Age { get; set; }
        public double Cost { get; set; }
        public int OperationResult { get; set; }

        public string Format() => $"{this.Type}\t{this.Age}\t{this.Cost},\t{this.OperationResult}";

        public static bool operator >(Pet lhs, Pet rhs)
        {
            return lhs.Age > rhs.Age;
        }

        public static bool operator <(Pet lhs, Pet rhs)
        {
            return lhs.Age < rhs.Age;
        }
    }

    public class PetStore
    {
        public Pet[] Pets { get; set; } = new Pet[50];
        private int Index = 0;
        public Pet this[int index] => Pets[index];
        public PetStore(string data)
        {
            Pets = JsonConvert.DeserializeObject<Pet[]>(data);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var pet in Pets)
            {
                if(pet != null)
                    sb.Append(pet.Format() + Environment.NewLine);
            }

            return sb.ToString();
        }
    }

    public enum Results
    {
        AllResults,
        NoResults,
        SomeResults
    }
}
