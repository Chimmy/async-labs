﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            var dir = Directory.GetCurrentDirectory();
            var outputDir = dir + "\\IFF711.AdomaitisP_rez.txt";

            var activeStore = (int)Results.AllResults;

            var petStores = Directory.GetFiles(dir, "*AdomaitisP_dat*")
                .Select(x => File.ReadAllText(x))
                .Select(x => new PetStore(x))
                .ToArray();

            var threadCount = petStores[0].Pets.Length / 3;
            var threads = new List<Thread>();

            var monitor = new ResultMonitor(p => p.Age > 8);

            foreach (var pet in petStores[activeStore].Pets)
            {
                monitor.Add(pet);
            }

            for (int i = 0; i < threadCount; i++)
            {
                var th = new Thread(() => Work(i, monitor));
                threads.Add(th);
                th.Start();
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }

            var res = monitor.FormatResults();

            var resultsFile = File.Create(outputDir);

            resultsFile.Write(Encoding.UTF8.GetBytes(res));
            resultsFile.Flush();

            Console.ReadKey();
        }

        public static void Work(int i, ResultMonitor monitor)
        {
            monitor.MoveToResults(i);
        }
    }
}
